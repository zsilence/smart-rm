#!/usr/bin/env python2.7

"""
smart rm installer
"""

from setuptools import setup, find_packages

setup(
    name="rms",
    version="1.0",
    packages=find_packages(),
    entry_points={
        "console_scripts": {
            "rms = rms.__main__:main"
        }
    },
    description="smart rm",
    author="Maksim Abramovich",
)


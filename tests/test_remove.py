import os
import unittest
import shutil
import tempfile

from rms.config import DEFAULT_CONFIG
from rms.trash import Trash


class RmTest(unittest.TestCase):

    def setUp(self):
        self.trash_path = tempfile.mkdtemp()
        self.log_path = os.path.join(tempfile.mkdtemp(), "rmlog.log")
        self.config = DEFAULT_CONFIG
        self.trash = Trash(
                           force=True,
                           trash_size=self.config["trash_size"],
                           remove_policy=self.config["remove_policy"],
                           clean_policy=self.config["clean_policy"],
                           restore_policy=self.config["restore_policy"],
                           log_path=self.log_path,
                           trash_path=self.trash_path,)
        self.temp_dir = tempfile.mkdtemp()
        os.chdir(self.temp_dir)

    def test_remove_file(self):

        open("file", "w").close()
        self.trash.remove("file")
        self.assertFalse(os.path.exists("file"))
        self.assertTrue(os.path.exists(os.path.join(self.trash.file_path, "file")))
        self.assertTrue(os.path.exists(os.path.join(self.trash.info_path, "file")))

    def test_remove_dir(self):

        os.mkdir("dir")
        self.trash.remove("dir")
        self.assertFalse(os.path.exists("dir"))
        self.assertTrue(os.path.exists(os.path.join(self.trash.file_path, "dir")))
        self.assertTrue(os.path.exists(os.path.join(self.trash.info_path, "dir")))

    def test_remove_big_size(self):

        self.trash.trash_size = 3
        with open("file", "w") as f:
            f.write("\0\0\0\0\0\0")

        self.trash.remove("file")
        self.assertFalse(os.path.exists("file"))
        self.assertFalse(os.path.exists(os.path.join(self.trash.file_path, "file")))
        self.assertFalse(os.path.exists(os.path.join(self.trash.info_path, "file")))

    def test_dryrun_remove(self):

        os.mkdir("dir")
        self.trash.dryrun = True
        self.trash.remove("dir")
        self.assertTrue(os.path.exists("dir"))
        self.assertFalse(os.path.exists(os.path.join(self.trash.file_path, "dir")))
        self.assertFalse(os.path.exists(os.path.join(self.trash.info_path, "dir")))

    def test_regex_remove(self):

        os.mkdir("dir")
        os.mkdir("dir/dir1")
        os.mkdir("dir/dir1/rm dir")
        open("dir/rm f", "w").close()
        open("dir/r1m", "w").close()
        open("dir/dir1/rm f2", "w").close()

        self.trash.regex_remove("dir", "rm")
        self.assertTrue(os.path.exists("dir/dir1"))
        self.assertTrue(os.path.exists("dir/r1m"))
        self.assertFalse(os.path.exists("dir/dir1/rm dir"))
        self.assertFalse(os.path.exists("dir/rm f"))
        self.assertFalse(os.path.exists("dir/dir1/rm f2"))

    def test_remove_in_trash(self):

        open("file", "w").close()
        self.trash.remove("file")

        self.trash.remove(os.path.join(self.trash.file_path, "dir"))
        self.trash.remove(os.path.join(self.trash.info_path, "dir"))
        self.trash.remove(self.trash.info_path)

        self.assertTrue(os.path.exists(os.path.join(self.trash.file_path, "file")))
        self.assertTrue(os.path.exists(os.path.join(self.trash.info_path, "file")))
        self.assertTrue(os.path.exists(self.trash.info_path))

    def test_remove_with_trash(self):

        os.mkdir("dir")
        os.mkdir("dir/new-trash")
        self.trash.trash_path = os.path.abspath("dir/new-path")

        try:
            self.trash.remove("dir")
        except:
            pass

        self.assertTrue(os.path.exists("dir"))

    def tearDown(self):

        os.chdir("..")
        shutil.rmtree(self.trash_path)
        shutil.rmtree(os.path.split(self.log_path)[0])
        shutil.rmtree(self.temp_dir)

if __name__ == '__main__':
    unittest.main()

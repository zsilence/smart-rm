import os
import unittest
import shutil
import tempfile

from rms.cleaner import delete_from_trash
from rms.config import DEFAULT_CONFIG
from rms.trash import Trash


class TrashTest(unittest.TestCase):

    def setUp(self):
        self.trash_path = tempfile.mkdtemp()
        self.log_path = os.path.join(tempfile.mkdtemp(), "rmlog.log")
        self.config = DEFAULT_CONFIG
        self.trash = Trash(
                           force=True,
                           trash_size=self.config["trash_size"],
                           remove_policy=self.config["remove_policy"],
                           clean_policy=self.config["clean_policy"],
                           restore_policy=self.config["restore_policy"],
                           log_path=self.log_path,
                           trash_path=self.trash_path,)
        self.temp_dir = tempfile.mkdtemp()
        os.chdir(self.temp_dir)

    def test_restore(self):

        os.mkdir("dir")
        self.trash.remove("dir")
        self.assertFalse(os.path.exists("dir"))
        self.trash.restore("dir")
        self.assertTrue(os.path.exists("dir"))
        self.assertFalse(os.path.exists(os.path.join(self.trash.file_path, "dir")))
        self.assertFalse(os.path.exists(os.path.join(self.trash.info_path, "dir")))

    def test_restore_to_nonexistent(self):

        os.mkdir("dir")
        os.mkdir("dir/dir1")
        open("dir/dir1/file", "w").close()

        self.trash.remove("dir/dir1/file")
        self.trash.remove("dir")
        self.trash.restore("file")

        self.assertTrue(os.path.exists("dir"))
        self.assertTrue(os.path.exists("dir/dir1"))
        self.assertTrue(os.path.exists("dir/dir1/file"))

    def test_dryrun_restore(self):

        os.mkdir("dir")
        self.trash.remove("dir")
        self.trash.dryrun = True
        self.trash.restore("dir")
        self.assertFalse(os.path.exists("dir"))
        self.assertTrue(os.path.exists(os.path.join(self.trash.file_path, "dir")))
        self.assertTrue(os.path.exists(os.path.join(self.trash.info_path, "dir")))

    def test_clean_trash(self):

        os.mkdir("dir")
        open("file", "w").close()
        self.trash.remove("dir")
        self.trash.remove("file")
        self.trash.clean_trash()
        self.assertEqual(len(os.listdir(self.trash.file_path)), 0)
        self.assertEqual(len(os.listdir(self.trash.info_path)), 0)

    def test_dryrun_clean(self):

        os.mkdir("dir")
        open("file", "w").close()
        self.trash.remove("dir")
        self.trash.remove("file")
        self.trash.dryrun = True
        self.trash.clean_trash()
        self.assertEqual(len(os.listdir(self.trash.file_path)), 2)
        self.assertEqual(len(os.listdir(self.trash.info_path)), 2)

    def test_delete_from_trash(self):

        open("file", "w").close()
        self.trash.remove("file")
        delete_from_trash(self.trash, "file")
        self.assertFalse(os.path.exists(os.path.join(self.trash.file_path, "file")))
        self.assertFalse(os.path.exists(os.path.join(self.trash.info_path, "file")))

    def test_dryrun_delete_from_trash(self):

        open("file", "w").close()
        self.trash.remove("file")
        self.trash.dryrun = True
        delete_from_trash(self.trash, "file")
        self.assertTrue(os.path.exists(os.path.join(self.trash.file_path, "file")))
        self.assertTrue(os.path.exists(os.path.join(self.trash.info_path, "file")))

    def tearDown(self):

        os.chdir("..")
        shutil.rmtree(self.trash_path)
        shutil.rmtree(os.path.split(self.log_path)[0])
        shutil.rmtree(self.temp_dir)

if __name__ == '__main__':
    unittest.main()

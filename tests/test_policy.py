import os
import unittest
import shutil
import tempfile

from rms.config import DEFAULT_CONFIG
from rms.trash import Trash


class PolicyTest(unittest.TestCase):

    def setUp(self):
        self.trash_path = tempfile.mkdtemp()
        self.log_path = os.path.join(tempfile.mkdtemp(), "rmlog.log")
        self.config = DEFAULT_CONFIG
        self.trash = Trash(
                           force=True,
                           trash_size=self.config["trash_size"],
                           remove_policy=self.config["remove_policy"],
                           clean_policy=self.config["clean_policy"],
                           restore_policy=self.config["restore_policy"],
                           log_path=self.log_path,
                           trash_path=self.trash_path,)
        self.temp_dir = tempfile.mkdtemp()
        os.chdir(self.temp_dir)

    def test_remove_replace(self):

        self.trash.remove_policy = "replace"
        open("file", "w").close()
        self.trash.remove("file")
        open("file", "w").close()
        self.trash.remove("file")

        self.assertEqual(len(os.listdir(self.trash.file_path)), 1)
        self.assertEqual(len(os.listdir(self.trash.info_path)), 1)
        self.assertEqual(os.listdir(self.trash.file_path)[0], "file")

    def test_remove_collision(self):

        self.trash.remove_policy = "collision"
        open("file", "w").close()
        self.trash.remove("file")
        open("file", "w").close()
        self.trash.remove("file")

        self.assertEqual(len(os.listdir(self.trash.file_path)), 2)
        self.assertEqual(len(os.listdir(self.trash.info_path)), 2)
        self.assertIn("file", os.listdir(self.trash.file_path))
        self.assertIn("file.1", os.listdir(self.trash.file_path))

    def test_restore_replace(self):

        self.trash.restore_policy = "replace"
        self.trash.remove_policy = "collision"
        open("file", "w").close()
        self.trash.remove("file")
        open("file", "w").close()
        self.trash.remove("file")
        self.trash.restore("file.1")
        self.trash.restore("file")

        self.assertEqual(len(os.listdir(self.temp_dir)), 1)
        self.assertEqual(len(os.listdir(self.temp_dir)), 1)
        self.assertEqual(os.listdir(self.temp_dir)[0], "file")

    def test_restore_collision(self):

        self.trash.remove_policy = "collision"
        self.trash.restore_policy = "collision"
        open("file", "w").close()
        self.trash.remove("file")
        open("file", "w").close()
        self.trash.remove("file")
        self.trash.restore("file.1")
        self.trash.restore("file")

        self.assertEqual(len(os.listdir(self.temp_dir)), 2)
        self.assertEqual(len(os.listdir(self.temp_dir)), 2)
        self.assertIn("file", os.listdir(self.temp_dir))
        self.assertIn("file.1", os.listdir(self.temp_dir))

    def test_clean_date(self):

        self.trash.clean_policy = "date"
        self.trash.trash_size = 3

        with open("file", "w") as f:
            f.write("\0")
        self.trash.remove("file")
        with open("file1", "w") as f:
            f.write("\0")
        self.trash.remove("file1")
        with open("file2", "w") as f:
            f.write("\0\0")
        self.trash.remove("file2")

        self.assertEqual(len(os.listdir(self.trash.file_path)), 2)
        self.assertIn("file1", os.listdir(self.trash.file_path))

    def test_clean_size(self):

        self.trash.clean_policy = "size"
        self.trash.trash_size = 4

        with open("file", "w") as f:
            f.write("\0")
        self.trash.remove("file")
        with open("file1", "w") as f:
            f.write("\0\0")
        self.trash.remove("file1")
        with open("file1", "w") as f:
            f.write("\0\0")
        self.trash.remove("file1")

        self.assertEqual(len(os.listdir(self.trash.file_path)), 2)
        self.assertIn("file", os.listdir(self.trash.file_path))

    def test_size(self):

        self.trash.trash_size = 1010101
        for i in xrange(8):
            with open("file"+str(i), "w") as f:
                f.write("\0\0\0\0\0\0\0\0\0\0\0\0\0\0")
            self.trash.remove("file"+str(i))

        self.trash.trash_size = 30
        with open("file new", "w") as f:
            f.write("\0")
        self.trash.remove("file new")

        self.assertEqual(len(os.listdir(self.trash.file_path)), 3)

    def tearDown(self):

        os.chdir("..")
        shutil.rmtree(self.trash_path)
        shutil.rmtree(os.path.split(self.log_path)[0])
        shutil.rmtree(self.temp_dir)

if __name__ == '__main__':
    unittest.main()

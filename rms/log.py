import logging
import sys


def create_logger(trash):
    """
    create loggers for user and for developer
    """
    file_logger = logging.getLogger("file_logger")
    file_logger.setLevel(logging.DEBUG)

    stream_logger = logging.getLogger("stream_logger")
    if trash.silent:
        stream_logger.setLevel(logging.CRITICAL)
    else:
        stream_logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(sys.stdout)
    stream_logger.addHandler(handler)

    handler = logging.FileHandler(trash.log_path)
    handler.setFormatter(logging.Formatter("%(levelname)-8s [%(asctime)s] %(message)s"))
    file_logger.addHandler(handler)

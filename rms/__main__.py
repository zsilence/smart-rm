import sys

from trash import Trash
from parser import parse
from config import init_configurations


def main():

    arguments = parse()

    configurations = init_configurations(arguments)

    t = Trash(dryrun=configurations["dryrun"],
              silent=configurations["silent"],
              interactive=configurations["interactive"],
              force=configurations["force"],
              trash_size=configurations["trash_size"],
              remove_policy=configurations["remove_policy"],
              clean_policy=configurations["clean_policy"],
              restore_policy=configurations["restore_policy"],
              log_path=configurations["log_path"],
              trash_path=configurations["trash_path"],
              )

    if arguments.command == "remove":
        if arguments.regex:
            for f in arguments.files:
                t.regex_remove(f, arguments.regex)
        else:
            for f in arguments.files:
                t.remove(f)

    if arguments.command == "restore":
        for f in arguments.files:
            t.restore(f)

    if arguments.command == "tr-delete":
        for f in arguments.files:
            t.delete_from_trash(f)

    if arguments.command == "show":
        t.show_trash()

    if arguments.command == "clean":
        t.clean_trash()

    return 0


if __name__ == "__main__":
    sys.exit(main())

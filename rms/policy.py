import os

from cleaner import delete


def name_by_policy(policy, path, replace_path):
    """
    get name for file in path by collision policy to replace it to replace_path
    """
    cnt = 0
    name = os.path.split(path)[1]
    if policy == "collision":
        new_name = name
        while os.path.lexists(os.path.join(replace_path, new_name)):
            cnt += 1
            new_name = name + "." + str(cnt)
        return new_name
    elif policy == "replace":
        return name
    else:
        raise IOError("unknown policy")


def move_by_policy(path, rename_path):
    """
    replace objects if set up
    """
    if os.path.lexists(rename_path):
        delete(rename_path)
    os.rename(path, rename_path)

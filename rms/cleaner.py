import os
import logging
import shutil

from helper import get_size

stream_logger = logging.getLogger("stream_logger")


def auto_clean(trash, path):
    """
    ask for auto clean trash if not enough space to remove path and start it by policy
    """
    if get_size(trash.file_path) + get_size(path) > trash.trash_size:
        if trash.force:
            ans = "y"
        else:
            ans = raw_input("run auto clean? [y/n] ").lower()
        if ans == "y":
            while get_size(trash.file_path) + get_size(path) > trash.trash_size:
                if trash.clean_policy == "size":
                    size_auto_clean(trash)
                elif trash.clean_policy == "date":
                    date_auto_clean(trash)
                else:
                    raise Exception("not correct policy clean")
        else:
            raise Exception("denied")


def date_auto_clean(trash):
    """
    delete the oldest file from trash
    """
    info_list = []
    for name in os.listdir(trash.info_path):
        with open(os.path.join(trash.info_path, name), "r") as info_file:
            info_list.append((info_file.read().split("\n")[1], name))
    info_list.sort(key=lambda t: t[0])
    trash.delete_from_trash(info_list[0][1])


def size_auto_clean(trash):
    """
    delete the biggest file from trash
    """
    file_list = os.listdir(trash.file_path)
    file_list.sort(key=lambda name: get_size(os.path.join(trash.file_path, name)), reverse=True)
    trash.delete_from_trash(file_list[0])

def big_size(trash, path):
    """
    remove object irretrievably if it exceed trash size
    """
    if get_size(path) > trash.trash_size:
        if trash.force:
            ans = "y"
        else:
            ans = raw_input("file size exceeds size of trash. delete irretrievably? [y/n] ").lower()
        if ans == "y":
            if not trash.dryrun:
                delete(path)
        else:
            raise Exception("denied")
        return True
    else:
        return False


def delete(path):
    """
    delete object
    """
    if os.path.isfile(path):
        os.remove(path)
    elif os.path.isdir(path):
        shutil.rmtree(path)

import os


TRASH_PATH = os.path.expanduser("~/trash")

CONFIG_PATH = os.path.expanduser("~/.rmsconfig")

DEFAULT_CONFIG = {"trash_size": 536870912,
                  "remove_policy": "replace",
                  "clean_policy": "date",
                  "restore_policy": "replace",
                  "trash_path": TRASH_PATH,
                  "log_path": os.path.join(TRASH_PATH, "rmlog.log")}

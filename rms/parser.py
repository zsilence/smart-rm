import argparse


def parse():
    """
    define command line argument, read it and return
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--dryrun", action="store_true", help="write about operations and do not perform him")
    parser.add_argument("-s", "--silent", action="store_true", help="do not show anything")
    parser.add_argument("-i", "--interactive", action="store_true", help="asks for confirmation of operation")
    parser.add_argument("-f", "--force", action="store_true", help="do not ask anything")

    parser.add_argument("--trash-path", action="store", help="path to trash")
    parser.add_argument("--trash-size", action="store", type=int, help="max size of trash")
    parser.add_argument("--log-path", action="store", help="path to log")
    parser.add_argument("--remove-policy", action="store", help="collision or replace remove policy")
    parser.add_argument("--restore-policy", action="store", help="collision or replace restore policy")
    parser.add_argument("--clean-policy", action="store", help="max size or min date clean policy")
    parser.add_argument("--config", action="store", help="config path for single usage")

    subparsers = parser.add_subparsers(title="subcommands",
                                       description="basic operations",
                                       help="description",
                                       dest="command")

    parser_remove = subparsers.add_parser("remove", help="remove file or directory")
    parser_remove.add_argument("files", action="store", nargs="*", help="files to remove")
    parser_remove.add_argument("--regex", action="store", help="remove objects by regular expression")

    parser_restore = subparsers.add_parser("restore", help="restore file or directory")
    parser_restore.add_argument("files", action="store", nargs="*", help="file or directory for restoring")

    parser_tr_delete = subparsers.add_parser("tr-delete", help="delete object from trash by name")
    parser_tr_delete.add_argument("files", action="store", nargs="*", help="file or directory for deleting")

    subparsers.add_parser("show", help="show objects from trash")

    subparsers.add_parser("clean", help="delete all objects from trash")

    return parser.parse_args()

import os
import logging

stream_logger = logging.getLogger("stream_logger")


def in_trash(trash, path):
    """
    raise exception if trying remove file from trash
    """
    if path.startswith(trash.trash_path):
        stream_logger.info("object is in trash")
        raise OSError("object is in trash")
    elif trash.trash_path.startswith(path):
        stream_logger.info("trash is in this directory")
        raise OSError("trash is in this directory")


def get_size(path):
    """
    return size of files in path
    """
    size = 0
    if os.path.isfile(path):
        return os.path.getsize(path)
    for dir_path, dir_names, file_names in os.walk(path):
        for f in file_names:
            size += os.path.getsize(os.path.join(dir_path, f))

    return size


def path_exist(path):
    """
    raise exception if path don't exist
    """
    if not os.path.lexists(path):
        stream_logger.info("path does not exist")
        raise OSError("path does not exist")


def ask(trash, text):
    """
    ask question from text and get answer
    """
    if trash.interactive:
        if raw_input(text+" [y/n] ").lower() == "y":
            return
        else:
            stream_logger.info("denied")
            raise Exception("denied")


def make_dir(path):
    """
     create new directory by path
    """
    if not os.path.lexists(os.path.abspath(path)):
        make_dir(os.path.split(path)[0])
        os.mkdir(path)


def access(trash, path):
    """
    check access to file and give it in force mode
    """
    if not os.access(path, os.W_OK):
        if not trash.dryrun and trash.force:
            os.chmod(path, 7777)
        else:
            stream_logger.info("access denied")
            raise OSError("access denied")

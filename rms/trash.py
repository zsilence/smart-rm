import os
import datetime
import re
import shutil
import logging
import helper

from log import create_logger
from cleaner import auto_clean, big_size, delete
from policy import name_by_policy, move_by_policy

file_logger = logging.getLogger("file_logger")
stream_logger = logging.getLogger("stream_logger")


class Trash(object):
    """
    class with trash basic functions
    """
    def __init__(self,
                 trash_size=50,
                 remove_policy="replace",
                 clean_policy="date",
                 restore_policy="replace",
                 log_path=os.devnull,
                 trash_path=os.path.expanduser("~"),
                 dryrun=False,
                 silent=False,
                 interactive=False,
                 force=False
                 ):
        self.log_path = log_path
        self.trash_path = os.path.join(trash_path, "trash")
        self.file_path = os.path.join(self.trash_path, "files")
        self.info_path = os.path.join(self.trash_path, "info")
        helper.make_dir(self.trash_path)
        helper.make_dir(self.file_path)
        helper.make_dir(self.info_path)
        self.dryrun = dryrun
        self.silent = silent
        self.interactive = interactive
        self.force = force
        self.trash_size = trash_size
        self.remove_policy = remove_policy
        self.clean_policy = clean_policy
        self.restore_policy = restore_policy
        create_logger(self)

    def remove(self, path):
        """
        remove object in path
        """
        try:
            real_path = os.path.abspath(path)
            helper.path_exist(real_path)
            helper.access(self, real_path)
            helper.in_trash(self, real_path)
            if big_size(self, real_path):
                stream_logger.info("file delete irretrievably")
            else:
                helper.ask(self, "remove {0} to trash?".format(real_path))
                new_name = name_by_policy(self.remove_policy, path, self.file_path)
                auto_clean(self, path)
                if not self.dryrun:
                    move_by_policy(path, os.path.join(self.file_path, new_name))
                    with open(os.path.join(self.info_path, new_name), "w") as info_file:
                        info_file.write(real_path+"\n")
                        info_file.write(str(datetime.datetime.now())+"\n")
                stream_logger.info("object {0} successfully removed as {1}".format(real_path, new_name))
            file_logger.info("object {0} successfully removed".format(real_path))
        except:
           file_logger.exception("removing file error")

    def regex_remove(self, path, regex):
        """
        remove objects in path by regex
        """
        try:
            helper.ask(self, "remove by regex {0} in {1}?".format(regex, path))
            file_list = os.listdir(os.path.abspath(path))
            for f in file_list:
                file_path = os.path.join(path, f)
                if re.search(regex, f):
                    self.remove(file_path)
                elif os.path.isdir(file_path):
                    self.regex_remove(file_path, regex)
            stream_logger.info("\nremove by regex {0} in {1} is successfully complete".format(regex, path))
            file_logger.info("regex remove successfully complete")
        except:
            file_logger.exception("error in regex remove")

    def clean_trash(self):
        """
        clean trash
        """
        try:
            helper.ask(self, "clean trash?")
            if not self.dryrun:
                shutil.rmtree(self.file_path)
                shutil.rmtree(self.info_path)
                helper.make_dir(self.file_path)
                helper.make_dir(self.info_path)
            stream_logger.info("trash is empty")
            file_logger.info("trash is empty")
        except:
            file_logger.exception("error in clean trash")

    def restore(self, name):
        """
        restore object from trash by name
        """
        try:
            helper.path_exist(os.path.join(self.file_path, name))
            helper.ask(self, "restore {0} from trash?".format(name))
            with open(os.path.join(self.info_path, name), "r") as info_file:
                restore_path = info_file.readline()[:-1]
            new_name = name_by_policy(self.restore_policy, restore_path, os.path.split(restore_path)[0])
            restore_path = os.path.join(os.path.split(restore_path)[0], new_name)
            if not self.dryrun:
                helper.make_dir(os.path.split(restore_path)[0])
                move_by_policy(os.path.join(self.file_path, name), restore_path)
                os.remove(os.path.join(self.info_path, name))
            stream_logger.info("object {0} successfully restored to {1}".format(name, restore_path))
            file_logger.info("object successfully restored to {0}".format(restore_path))
        except:
            file_logger.exception("restoring file error")

    def show_trash(self):
        """
        show trash content
        """
        try:
            for f in os.listdir(self.file_path):
                stream_logger.info(f)
            stream_logger.info("\ntrash is showed")
            file_logger.info("trash is showed")
        except:
            file_logger.exception("error in show trash")

    def delete_from_trash(self, name):
        """
        delete file from trash by name
        """
        try:
            if not self.dryrun:
                info_file = os.path.join(self.info_path, name)
                trash_object = os.path.join(self.file_path, name)
                if not self.dryrun:
                    os.remove(info_file)
                    delete(trash_object)

            stream_logger.info("object {0} deleted from trash".format(name))
            file_logger.info("object deleted from trash")
        except:
            file_logger.info("error in deleting file from trash")

import json
import os

from setting import DEFAULT_CONFIG, CONFIG_PATH


def update_config(arguments, config=DEFAULT_CONFIG):
    """
    return config updated by command line arguments
    """
    args = vars(arguments)

    for k in DEFAULT_CONFIG:
        if args[k] is None:
            del args[k]

    config.update(args)

    return config


def read_config():
    """
    if config file exists read it else create it
    """
    if not os.path.exists(CONFIG_PATH):
        with open(CONFIG_PATH, "w") as f:
            json.dump(DEFAULT_CONFIG, f, indent=4, separators=(',', ': '))
        config = DEFAULT_CONFIG
    else:
        with open(CONFIG_PATH) as f:
            config = json.load(f)

    return config


def init_configurations(arguments):
    """
    initialise configurations for usage
    """
    if arguments.config is not None:
        with open(arguments.config) as f:
            custom_config = json.load(f)
        custom_config = update_config(arguments, custom_config)
    else:
        config = read_config()
        custom_config = update_config(arguments, config)

    return custom_config
